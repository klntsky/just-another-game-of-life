char fold;
int test (unsigned char *rule_map);
unsigned char get(unsigned char* ptr, int w, int h, int x, int y);
unsigned char get_nbs(unsigned char *ptr, int w, int h, int x, int y);
void set(unsigned char* ptr, int w, int h, int x, int y, unsigned char v);
void print(unsigned char* ptr, int w, int h);
void exit_eof();
void read_field (FILE* ifd, unsigned char* ptr, int w, int h);
void populate_map (unsigned char *rule_map,
                   unsigned char (*oracle)(unsigned char, unsigned char));
unsigned char apply (unsigned char *rule_map, unsigned char state, unsigned char c);
unsigned char conway_oracle (unsigned char state, unsigned char c);
