.PHONY: build test

CC = gcc
CFLAGS = --std=c89 --ansi -Wall
build: main.c
	${CC} ${CFLAGS} main.c life.c -o life

test: test.c
	${CC} ${CFLAGS} test.c life.c -o test
