#!/bin/bash

make;

for file in examples/*;
do
    clear;
    echo "Example: $file";
    sleep 2;
    ./life "$file" &
    pid="$!"
    sleep 8;
    kill $pid
done
