#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "life.h"

unsigned char get(unsigned char* ptr, int w, int h, int x, int y) {
  if (fold) {
    x = x % w;
    y = y % h;

    if (y < 0) {
      y = h + y;
    }
    if (x < 0) {
      x = w + x;
    }
  } else {
    if (x < 0 || x >= w) {
      return 0;
    }
    if (y < 0 || y >= h) {
      return 0;
    }
  }

  size_t shift = w * y + x;
  char rel_shift = shift % 8;
  char c = ptr[shift / 8];
  return (c >> rel_shift) & 1;
}

unsigned char get_nbs(unsigned char *ptr, int w, int h, int x, int y) {
  int i, j;
  unsigned char c = 0;

  for (j = -1; j < 2; j++) {
    for (i = -1; i < 2; i++) {
      if (i == 0 && j == 0)
        continue;
      c = c * 2;
      c = c + get(ptr, w, h, x + i, y + j);
    }
  }
  return c;
}

void set(unsigned char* ptr, int w, int h, int x, int y, unsigned char state) {
  if (state > 1) {
    exit(1);
  }

  size_t shift = w * y + x;
  unsigned char rel_shift = shift % 8;
  unsigned char c = ptr[shift / 8];
  unsigned char mask = 1 << rel_shift;

  if (state == 0) {
    ptr[shift / 8] = c & (~ mask);
  } else {
    ptr[shift / 8] = c | mask;
  }
}

void print(unsigned char* ptr, int w, int h) {
  unsigned char c;
  int x, y;

  for (x = 0; x < w; x++) {
    for (y = 0; y < h; y++) {
      c = get(ptr, w, h, x, y);
      if (c == 0) {
        printf("_");
      } else {
        printf("#");
      }
    }
    printf("\n");
  }
}


void read_field (FILE* ifd, unsigned char* ptr, int w, int h) {
  int x; int y; unsigned char c;

  for (x = 0; x < w; x++) {
    /* consume whitespace or newline */
    if (!feof(ifd)) {
      fscanf(ifd, " ");
    }
    else
      exit_eof();

    for (y = 0; y < h; y++) {

      if (!feof(ifd))
        c = fgetc(ifd);
      else
        exit_eof();

      if (c != '_') {
        set(ptr, w, h, x, y, 1);
      }
    }
  }
}

/* Populates rule_map using given function that maps nearby cells' bitmaps
   to {0, 1}
 */
void populate_map (unsigned char *rule_map,
                   unsigned char (*oracle)(unsigned char, unsigned char)) {
  unsigned char c, mask, r; int i, state;

  for (state = 0; state <= 1; state++) {
    for (i = 0; i < 256; i++) {
      c = rule_map[(state << 5) | (i / 8)];
      r = (*oracle)(state, i);
      if (r == 0) {
        mask = 0;
      } else {
        mask = 1 << (i % 8);
      }
      rule_map[(state << 5) | (i / 8)] = c | mask;
    }
  }
}

/* Apply rule from rule_map to state and bitmap of neighbours. */
unsigned char apply (unsigned char *rule_map, unsigned char state, unsigned char nbs) {
  unsigned char ix = nbs / 8;
  unsigned char shift = nbs % 8;
  return (rule_map[(state << 5) | ix] >> shift) % 2;
}

/* Accepts cell's state (0 or 1) and heighbours' bitmap as a single uchar and decides whether the in-between cell
   should be dead or alive (0 or 1) in the next generation. */
unsigned char conway_oracle (unsigned char state, unsigned char nbs) {
  unsigned char bit_sum = 0, i;

  if (state) {
    for (i = 0; i < 8; i++) {
      bit_sum += nbs & 1;
      /* If bit_sum is > 3, cell will be dead -
         no need to iterate more. */
      if (bit_sum >> 2) {
        return 0;
      }
      nbs = nbs >> 1;
    }

    /* At this point there are only four possibilities left.

       bit_sum      n      return value

       0b0000000    0       0
       0b0000001    1       0
       0b0000010    2       1
       0b0000011    3       1
              ^             ^
       so we just shift bit_sum to the right by one bit.
    */

    return bit_sum >> 1;
  } else {
    for (i = 0; i < 8; i++) {
      bit_sum += nbs & 1;
      nbs = nbs >> 1;
    }
    return bit_sum == 3;
  }
}

void exit_eof() {
  printf("EOF while reading input file.\n");
  exit(4);
}
