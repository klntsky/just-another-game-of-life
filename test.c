#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>

#include "life.h"

int main (int argc, char** argv) {
  unsigned char *rule_map;

  rule_map = (unsigned char *) malloc(64);
  memset(rule_map, 0, 64);
  populate_map(rule_map, conway_oracle);

  test(rule_map);
  printf("All assertions passed!\n");
  return 0;
}

int test (unsigned char *rule_map) {
  /* Testing oracle */
  int i;

  /* Underpopulation (< 2) */
  assert(apply(rule_map, 1, 0b00000000) == 0);
  assert(apply(rule_map, 1, 0b10000000) == 0);
  assert(apply(rule_map, 1, 0b01000000) == 0);
  assert(apply(rule_map, 1, 0b00100000) == 0);
  assert(apply(rule_map, 1, 0b00010000) == 0);
  assert(apply(rule_map, 1, 0b00001000) == 0);
  assert(apply(rule_map, 1, 0b00000100) == 0);
  assert(apply(rule_map, 1, 0b00000010) == 0);
  assert(apply(rule_map, 1, 0b00000001) == 0);

  /* Reproduction (2 to 3)*/
  assert(apply(rule_map, 1, 0b00000011) == 1);
  assert(apply(rule_map, 1, 0b10000001) == 1);
  assert(apply(rule_map, 1, 0b10000001) == 1);
  assert(apply(rule_map, 1, 0b01000001) == 1);
  assert(apply(rule_map, 1, 0b01000010) == 1);
  assert(apply(rule_map, 1, 0b01100000) == 1);

  assert(apply(rule_map, 1, 0b10000011) == 1);
  assert(apply(rule_map, 1, 0b00010011) == 1);
  assert(apply(rule_map, 1, 0b10100001) == 1);
  assert(apply(rule_map, 1, 0b10000101) == 1);
  assert(apply(rule_map, 1, 0b01001001) == 1);
  assert(apply(rule_map, 1, 0b01100010) == 1);
  assert(apply(rule_map, 1, 0b11100000) == 1);
  assert(apply(rule_map, 1, 0b00001011) == 1);

  /* Overpopulation (> 3) */
  assert(apply(rule_map, 1, 0b11110000) == 0);
  assert(apply(rule_map, 1, 0b11111111) == 0);
  assert(apply(rule_map, 1, 0b00001111) == 0);
  assert(apply(rule_map, 1, 0b10110010) == 0);
  assert(apply(rule_map, 1, 0b10101010) == 0);
  assert(apply(rule_map, 1, 0b01010101) == 0);
  assert(apply(rule_map, 1, 0b11010101) == 0);
  assert(apply(rule_map, 1, 0b11010111) == 0);
  assert(apply(rule_map, 1, 0b11011111) == 0);

  /* Survival (==3) */
  assert(apply(rule_map, 0, 0b11110000) == 0);
  assert(apply(rule_map, 0, 0b11111111) == 0);
  assert(apply(rule_map, 0, 0b00001111) == 0);
  assert(apply(rule_map, 0, 0b10110010) == 0);
  assert(apply(rule_map, 0, 0b10101010) == 0);
  assert(apply(rule_map, 0, 0b01010101) == 0);
  assert(apply(rule_map, 0, 0b11010101) == 0);
  assert(apply(rule_map, 0, 0b11010111) == 0);
  assert(apply(rule_map, 0, 0b11011111) == 0);

  assert(apply(rule_map, 0, 0b00000000) == 0);
  assert(apply(rule_map, 0, 0b11111111) == 0);
  assert(apply(rule_map, 0, 0b01111111) == 0);
  assert(apply(rule_map, 0, 0b00111111) == 0);
  assert(apply(rule_map, 0, 0b00011111) == 0);
  assert(apply(rule_map, 0, 0b00001111) == 0);

  assert(apply(rule_map, 0, 0b00000111) == 1);
  assert(apply(rule_map, 0, 0b10000011) == 1);
  assert(apply(rule_map, 0, 0b10101000) == 1);
  assert(apply(rule_map, 0, 0b00101010) == 1);
  assert(apply(rule_map, 0, 0b00111000) == 1);

  assert(apply(rule_map, 0, 0b00000011) == 0);
  assert(apply(rule_map, 0, 0b00000001) == 0);

  for (i = 0; i < 256; i++) {
    assert(conway_oracle(1, i) == apply(rule_map, 1, i));
    assert(conway_oracle(0, i) == apply(rule_map, 0, i));
  }

  int h = 3; int w = 3; unsigned int byte_size;
  int j;
  byte_size = h * w / 8 + 1;
  unsigned char * field = (unsigned char *) malloc(byte_size);
  memset(field, 0, byte_size);

  /* Neighbours of X get visited in the following order:

     123
     4X5
     678

  */

  /* Test 1:
     ___
     ___
     ___

   */
  for (i = 0; i < w; i++) {
    for (j = 0; j < h; j++) {
      assert(get_nbs(field, w, h, i, j) == 0b00000000);
    }
  }

  /* Test 2:
     #__
     _#_
     ___ */

  set(field, w, h, 0, 0, 1);
  set(field, w, h, 1, 1, 1);

  assert(get_nbs(field, w, h, 1, 1) == 0b10000000);
  assert(get(field, w, h, -3, 0) == fold);
  assert(get(field, w, h, -3, -3) == fold);
  assert(get(field, w, h, 0, -3) == fold);

  /* Test 3:
     ##_
     #__
     ___ */

  memset(field, 0, byte_size);
  set(field, w, h, 0, 0, 1);
  set(field, w, h, 1, 0, 1);
  set(field, w, h, 0, 1, 1);
  assert(get_nbs(field, w, h, 1, 1) == 0b11010000);
  assert(get_nbs(field, w, h, 0, 0) == 0b00001010);


  /* Test 4:
     ###
     ###
     ### */

  memset(field, 0, byte_size);

  for (i = 0; i < w; i++) {
    for (j = 0; j < h; j++) {
      set(field, w, h, i, j, 1);
    }
  }

  if (fold) {
    for (i = -100; i < 100; i++) {
      for (j = -100; j < 100; j++) {
        assert(get_nbs(field, w, h, i, j) == 0b11111111);
      }
    }
  }

  return 1;
}
