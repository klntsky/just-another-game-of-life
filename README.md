# Just Another Game of Life Implementation

## Input file format

On the first line there should be three numbers, specifying:

- whether to fold the field into a tor (i.e. cells from the outside of the visible field will be considered to reside on the opposite side).

- field height

- field width

Starting from the second line the field itself should be defined using characters '#' and '_' for "alive" and "dead" states, respectively. Field size is not being checked, so correct values should be specified. Additional whitespaces at the line ends are OK.

### Example:

```
1 6 6
______
______
__##__
__##__
______
______
```
