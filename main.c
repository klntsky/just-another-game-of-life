#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "life.h"

char fold = 0;

void usleep(int);

int main (int argc, char** argv) {
  unsigned char *field1;
  unsigned char *field2;
  unsigned char *tmp;
  int h; int w; int x, y;
  size_t byte_size;
  FILE *ifd;
  unsigned char *rule_map;

  /* 1 bit for each rule. 1 cell + 8 cells around => 2 ^ (8 + 1) rules =>
     => 512 / 8 bits per byte = 64 bytes */
  rule_map = (unsigned char *) malloc(64);
  memset(rule_map, 0, 64);
  populate_map(rule_map, conway_oracle);

  if (argc != 2) {
    printf("First argument should be input file!\n");
    exit(2);
  }

  ifd = fopen(argv[1], "r");
  fscanf(ifd, "%hhu", &fold);
  fscanf(ifd, "%d", &w);
  fscanf(ifd, "%d", &h);
  byte_size = h * w / 8 + 1;
  field1 = (unsigned char *) malloc(byte_size);
  field2 = (unsigned char *) malloc(byte_size);
  memset(field1, 0, byte_size);
  memset(field2, 0, byte_size);
  read_field(ifd, field1, w, h);

  while (1) {
    /* Clear screen */
    printf("\x1b\x5b\x48\x1b\x5b\x32\x4a");
    print(field1, w, h);

    for (y = 0; y < h; y++) {
      for (x = 0; x < w; x++) {
        set(field2, w, h, x, y,
            apply(rule_map, get(field1, w, h, x, y),
                  get_nbs(field1, w, h, x, y)));
      }
    }

    tmp = field1;
    field1 = field2;
    field2 = tmp;
    usleep(10000);
  }

  /* Free resources - will never be executed because of the infinite loop,
     though. */
  fclose(ifd);
  free(field1);
  free(field2);
  free(rule_map);
  return 0;
}
